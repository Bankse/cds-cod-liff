import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About'
import PromotionDetail from './views/promotion/Detail'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: 'Chat & Shop :: Catalog Showcase'
      }
    },
    {
      path: '/detail/:id',
      name: 'promotiondetail',
      component: PromotionDetail,
      meta: {
        title: 'Chat & Shop :: Catalog Showcase Detail'
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        title: 'Chat & Shop :: Catalog Showcase'
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(!to.meta.requiresAuth)
  {
    document.title = to.meta.title;
    next();
  }
})

export default router
