FROM node:8.11.3-alpine as node

WORKDIR /usr/app
COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn build

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine
COPY --from=node /usr/app/dist/ /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf